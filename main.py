def help():
    print("git is a source code versioning tool")
    print("it solves many problems that arise when coding (with other people)")
    print("it also helps when you make a mistake that breaks the whole code base")
    print()
    print("git was developed by Linus Torvalds the man who created the Linux kernel")
    print("and said the most true words ;)")
    print('\t"A computer is like an AC in a room; as soon as you open windows it becomes useless"')
    print("as well as")
    print('\t"Fuck you nVidia, fuck you!"')
    print()
    print("You can use git only on the command line (CLI) like a pro")
    print("\tgit [add|commit|pull|fetch|push|merge] [options]")
    print()
    print("Good commit messages are essential too, take a look at")
    print("\thttps://www.conventionalcommits.org/en/v1.0.0/")
    print()
    # TODO: complete

if __name__ == "__main__":
    print("git-etut")
    print()
    print("\t--help...shows help for git")
